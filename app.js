const search = require('./routes/search')
const login = require('./routes/login')
const profile = require('./routes/profile')
const SpotifyWebApi = require('spotify-web-api-node');
const express = require('express');
const mongoose = require('mongoose');
const mongoReference = "mongodb+srv://SpotifyLearn:Steven09@spotifylearn.mbpdv.mongodb.net/test"

   mongoose.connect(mongoReference, {useNewUrlParser: true, useUnifiedTopology: true})
           .then( () => console.log('Success conection with database'))
           .catch( error => console.log('Error conection with database', error))

   /** Express init */
   const app = express()
   app.use(express.json());
   app.use(express.urlencoded({extended: true}))
   app.use('/v1/search', search)
   app.use('/v1/login', login)
   app.use('/v1/profile', profile)

   /** Listen port API*/
   const port = process.env.PORT || 3000;
   app.listen(port, () =>{
       console.log('Success API conection');
   })
   
   
